CREATE DATABASE "practice_library_mgtSys"
    WITH 
    OWNER = postgres
    ENCODING = 'UTF8'
    CONNECTION LIMIT = -1;

CREATE TABLE public.publisher
(
    pub_id serial NOT NULL,
    pub_name character varying(255) NOT NULL,
    pub_address character varying(255),
    CONSTRAINT publisher_pkey PRIMARY KEY (pub_id),
)

TABLESPACE pg_default;

CREATE TABLE public.book
(
    book_id serial NOT NULL,
    title character varying(255),
	author_name character varying(255),
	stock int,
    CONSTRAINT book_pkey PRIMARY KEY (book_id)
)

TABLESPACE pg_default;

CREATE TABLE public.published_by
(
    pub_id serial NOT NULL,
	book_id serial NOT NULL,
	CONSTRAINT fk_publisher FOREIGN KEY (pub_id) REFERENCES publisher (pub_id),
	CONSTRAINT fk_book FOREIGN KEY (book_ID) REFERENCES book (book_id)
)

TABLESPACE pg_default;

CREATE TABLE public.members
(
	member_id serial NOT NULL,
	member_name character varying(255) NOT NULL,
	member_address character varying(255),
	member_date date NOT NULL,
    exp_date date NOT NULL,
	CONSTRAINT member_pkey PRIMARY KEY (member_id)
)

TABLESPACE pg_default;

CREATE TABLE public.borrowed_by
(
	borrow_id serial NOT NULL,
	member_id int NOT NULL,
	book_id int NOT NULL,
	borrow_date date NOT NULL,
    due_date date NOT NULL,
    return_date date,
	fine int,
    active_status boolean,
	CONSTRAINT borrow_pkey PRIMARY KEY (borrow_id),
	CONSTRAINT fk_member_id FOREIGN KEY (member_id) REFERENCES members (member_id),
    CONSTRAINT fk_book_id FOREIGN KEY (book_id) REFERENCES book (book_id)
)

TABLESPACE pg_default;

CREATE FUNCTION public.borrow_date()
    RETURNS trigger
    LANGUAGE 'plpgsql'
     NOT LEAKPROOF
AS $BODY$
BEGIN
    NEW.borrow_date = CURRENT_DATE;
    RETURN NEW;
END
$BODY$;

ALTER FUNCTION public.borrow_date()
    OWNER TO postgres;

CREATE TRIGGER borrow_date
    BEFORE INSERT
    ON public.borrowed_by
    FOR EACH ROW
    EXECUTE PROCEDURE public.borrow_date();

CREATE FUNCTION public.due_date()
    RETURNS trigger
    LANGUAGE 'plpgsql'
     NOT LEAKPROOF
AS $BODY$
BEGIN
    NEW.due_date = CURRENT_DATE + 14;
    RETURN NEW;
END
$BODY$;

ALTER FUNCTION public.due_date()
    OWNER TO postgres;

CREATE TRIGGER due_date
    BEFORE INSERT
    ON public.borrowed_by
    FOR EACH ROW
    EXECUTE PROCEDURE public.due_date();

CREATE FUNCTION public.member_date()
    RETURNS trigger
    LANGUAGE 'plpgsql'
     NOT LEAKPROOF
AS $BODY$
BEGIN
    NEW.member_date = CURRENT_DATE;
    RETURN NEW;
END
$BODY$;

ALTER FUNCTION public.member_date()
    OWNER TO postgres;

CREATE TRIGGER member_date
    BEFORE INSERT
    ON public.members
    FOR EACH ROW
    EXECUTE PROCEDURE public.member_date();

CREATE FUNCTION public.exp_date()
    RETURNS trigger
    LANGUAGE 'plpgsql'
     NOT LEAKPROOF
AS $BODY$
BEGIN
    NEW.exp_date = CURRENT_DATE + 365;
    RETURN NEW;
END
$BODY$;

ALTER FUNCTION public.exp_date()
    OWNER TO postgres;

CREATE TRIGGER exp_date
    BEFORE INSERT
    ON public.members
    FOR EACH ROW
    EXECUTE PROCEDURE public.exp_date();

CREATE FUNCTION public.fine()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
BEGIN
	NEW.fine = CONVERT(int,borrowed_by.return_date - borrowed_by.due_date)*10000;
	RETURN NEW;
END
$BODY$;

ALTER FUNCTION public.fine()
    OWNER TO postgres;

CREATE TRIGGER fine
    BEFORE UPDATE OF return_date
    ON public.borrowed_by
    FOR EACH ROW
    EXECUTE PROCEDURE public.fine();

CREATE FUNCTION public.stock_update()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
BEGIN
	UPDATE book as b
	SET stock = stock - (
		SELECT count(brb.book_id) 
		FROM borrowed_by as brb
		WHERE brb.book_id = b.book_id AND brb.active_status = 1)
	WHERE b.book_id = brb.book_id;
END
$BODY$;

ALTER FUNCTION public.stock_update()
    OWNER TO postgres;

CREATE TRIGGER stock_update
    BEFORE INSERT OR UPDATE OF book_id
    ON public.borrowed_by
    FOR EACH ROW
    EXECUTE PROCEDURE public.stock_update();